# DontDestoryOnLoad_Ext_Unity
A simple extension utility class to resolve that Unity does not expose GameObjects list tagged **DontDestroyOnLoad**.

## Note
Please add "DDOL_EXT" to your scripting define symbols in your Unity Project Settings if you want possible plugins to know this system is used.